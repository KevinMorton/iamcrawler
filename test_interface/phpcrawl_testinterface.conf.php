<?php
#####################################################
#
# Configuration-file for the phpcrawl testinterface
#
#####################################################

// Username and password for the test-interface-login.
$cfg_authUsername = "";
$cfg_authPassword = "";

#####################################################

// This follwoing options should be okay like they are.

// Where to save your setups.
// Be sure the script is allowed to
// write to this diretory.
$cfg_setupSaveDir = "./setups/";

// Where to find the phpcrawl-classes.
// (IAMCrawler.class.php and IAMCrawlerutils.class.php)
$cfg_phpcrawlClassDir = "../classes/";

// And where to find the phpcrawl-class-documentation ?
// (classreference)?
$cfg_phpcrawlClassrefPage = "../documentation/classreferences/IAMCrawler/method_detail_tpl_method_";
?>